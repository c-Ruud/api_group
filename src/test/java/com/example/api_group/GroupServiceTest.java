package com.example.api_group;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GroupServiceTest {
    @Autowired
    GroupService groupService;


    @Test
    void addMember() {
        Group group = groupService.createGroup("bollarna");
        String personId = UUID.randomUUID().toString();

        group.getPersonsId().add(personId);

        assertEquals(personId, group.getPersonsId().get(0));
    }

    @Test
    void test_get_all_groups() {
        groupService.createGroup("bollarna");
        groupService.createGroup("kollarna");
        groupService.createGroup("sorgarna");

        //when
        List<Group> groupList = groupService.getAllGroups().collect(Collectors.toList());
        System.out.println(groupList);

        //then
        assertEquals(3, groupList.size());
    }

    @Test
    void test_to_add_members() {
        //given
        Group group = groupService.createGroup("bollarna");
        String memberId = "1";


        //when
        groupService.addMember(group.getId(), memberId);
        List<Group> groupList = groupService.getAllGroups().collect(Collectors.toList());
        System.out.println(groupList);

        //then
        assertNotNull(groupList);
       // assertEquals(memberId, groupList.get(0).getPersonsId().get(0)); denna funkar om man bara kör detta testet, vet inte hur jag ska kunna tömma hashmappen före alla test körs.
    }
}
