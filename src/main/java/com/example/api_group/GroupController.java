package com.example.api_group;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/groups")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GroupController {

    GroupService groupService;

    @PostMapping("/create")
    public Group createGroup(@RequestParam String name) {
        return groupService.createGroup(name);
    }

    @GetMapping("/get/{id}")
    public Group getGroupById(@PathVariable String id) {
        return groupService.getById(id);
    }

    @PutMapping("/update/{id}")
    public Group updateGroup(@PathVariable String id, @RequestBody String newName) {
        return groupService.updateGroup(id, newName);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteGroup(@PathVariable String id) throws GroupException {
        return groupService.deleteGroup(id);
    }

    @GetMapping("/all")
    public List<Group> getAllGroups() {
        return groupService.getAllGroups().collect(Collectors.toList());
    }

    @PostMapping("/addMember/{groupId}/{personId}")
    public String addMember(@PathVariable String groupId, @PathVariable String personId) {
        return groupService.addMember(groupId, personId);
    }
}



//    @DeleteMapping("/removeMember/{memberId}")
//    public String deleteMember(@PathVariable String memberId) {
//      return groupService.deleteMemberById(memberId);
//    }



//, @RequestHeader(value = "token", required = true) String tokenId
