package com.example.api_group;

public class GroupException extends Exception {
    public GroupException(String message){
        super(message);
    }
}
