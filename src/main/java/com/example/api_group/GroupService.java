package com.example.api_group;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class GroupService {
    Map<String, Group> groups = new HashMap<>();

    public Group createGroup(String name) {
        String id = UUID.randomUUID().toString();
        Group group = new Group(id, name, new ArrayList<>());
        groups.put(id, group);
        return group;
    }

    public Stream<Group> getAllGroups() {
        System.out.println(groups);
        return groups.values().stream();
    }

    public Group getById(String id) {
        return groups.get(id);
    }

    public Group updateGroup(String id, String newName) {
        Group group = groups.get(id);
        group.setName(newName);
        return group;
    }

    public String deleteGroup(String id) throws GroupException {
        boolean doExist = groups.containsKey(id);
        if (!doExist) throw new GroupException("Group not found");
        groups.remove(id);
        return "Group with id: " + id + " was successfully deleted";
    }

    public String addMember(String groupId, String personId) {
        Group group = groups.get(groupId);
        group.getPersonsId().add(personId);
        return "Person added to group: " + group.getName();
    }
}


//    public String deleteMemberById(String memberId) {
//        groups.forEach(((s, group) -> group.deleteMember(memberId)));
//        return "Member was successfully deleted";
//    }


